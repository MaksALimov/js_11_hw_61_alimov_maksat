import React, {useEffect, useState} from 'react';
import axios from "axios";
import './RickAndMorty.css';

const RickMortyUrl = 'https://rickandmortyapi.com/api/character'

const RickAndMorty = () => {
    const [game, setGame] = useState([]);
    const [page, setPage] = useState(1);

    useEffect(() => {
        const rickAndMortyData = async () => {
          const response = await axios.get(RickMortyUrl + '/?page=' + page);
          console.log(response.data.results);
          setGame(response.data.results)
        };

        rickAndMortyData().catch(e => console.log(e));
    }, [page]);


    return (
        <div>
            {game.map((character => (
                <div key={character.id} className="Character-wrapper">
                    <h4>Name: {character.name}</h4>
                    <p>Gender: {character.gender}</p>
                    <p>Status: {character.status}</p>
                    <p>Location: {character.location.name}</p>
                    <img src={character.image} alt="character"/>
                </div>
            )))}
            <div className="Buttons-Wrapper">
                <button onClick={() => setPage(prevStata => prevStata - 1)} className="Previous">Previous</button>
                <button onClick={() => setPage(prevStata => prevStata + 1)} className="Next">Next</button>
            </div>
        </div>
    );
};

export default RickAndMorty;