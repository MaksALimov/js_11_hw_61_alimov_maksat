import React from 'react';
import './App.css';
import AllCountryData from "./containers/AllCountryData/AllCountryData";

const App = () => (
    <AllCountryData/>
);

export default App;
