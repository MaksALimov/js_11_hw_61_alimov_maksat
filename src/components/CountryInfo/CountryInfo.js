import React from 'react';
import CountryBorders from "../CountryBorders/CountryBorders";
import './CountryInfo.css';

const CountryInfo = ({oneCountry, countryBorders}) => {
    return (
        <div className="Country-info">
            {oneCountry.length === 0 ? <span className="chooseCountry">Выберите страну</span> :
                oneCountry.map(country => (
                    <ul key={country.alpha3Code}>
                        <li><b>Country</b>: {country.name}</li>
                        <li><b>Capital</b>: {country.capital}</li>
                        <li><b>Population</b>: {country.population}</li>
                        <li><b>Region</b>: {country.region}</li>
                        <li><b>Area</b>: {country.area}</li>
                        <li>
                            <img src={country.flag} alt="flag"/>
                        </li>
                    </ul>
                ))
            }
            <CountryBorders countryBorders={countryBorders}/>
        </div>
    );
};

export default CountryInfo;