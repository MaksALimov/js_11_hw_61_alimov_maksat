import React from 'react';
import './CountryBorders.css'

const CountryBorders = ({countryBorders}) => {
    return (
        <div>
            {countryBorders.length > 0 ?
                <p>
                    Borders with:
                </p> : null}

            {countryBorders.map(country => (
                    <ul
                        key={country.alpha3Code}>
                        <li>
                            {country.name}
                        </li>
                    </ul>
                )
            )}
        </div>
    );
};

export default CountryBorders;