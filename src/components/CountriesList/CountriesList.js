import React from 'react';
import './CountriesList.css';

const CountriesList = ({countries, onCountryClick}) => {
    return (
        <div className="Countries">
            {countries.map((country) => (
                <ul key={country.alpha3Code} className='Countries__list'>
                    <li onClick={() => onCountryClick(country.alpha3Code)}>{country.name}</li>
                </ul>
            ))}
        </div>
    );
};

export default CountriesList;