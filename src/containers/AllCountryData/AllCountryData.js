import React, {useCallback, useEffect, useState} from 'react';
import axios from "axios";
import CountriesList from "../../components/CountriesList/CountriesList";
import CountryInfo from "../../components/CountryInfo/CountryInfo";
import RickAndMorty from "../../RickAndMorty";

const countriesUrl = 'https://restcountries.eu/rest/v2/all?fields=name;alpha3Code';
const oneCountryUrl = 'https://restcountries.eu/rest/v2/alpha/';
const countryBordersUrl = 'https://restcountries.eu/rest/v2/alpha/';

const AllCountryData = () => {
    const [countries, setCountries] = useState([]);
    const [oneCountry, setOneCountry] = useState([]);
    const [countryBorders, setCountryBorders] = useState([]);

    useEffect(() => {
        const axiosData = async () => {
            const response = await axios.get(countriesUrl);

            setCountries(response.data);
        };

        axiosData().catch(e => console.error(e.message));
    }, []);


    const onCountryClick = useCallback(async (alphaCode) => {
        if (alphaCode === undefined) return;

        const response = await axios.get(oneCountryUrl + alphaCode);

        const countryBorders = response.data.borders;

        const responses = await Promise.all(countryBorders.map(border => axios.get(countryBordersUrl + border)));

        setOneCountry([response.data]);

        setCountryBorders(responses.map(country => {
            return country.data
        }));
    }, []);


    useEffect(() => {
        onCountryClick().catch(e => console.log(e));
    }, [onCountryClick]);

    return (
       <div>
           <div className="Wrapper">
               <CountriesList countries={countries} onCountryClick={onCountryClick}/>
               <CountryInfo oneCountry={oneCountry} countryBorders={countryBorders}/>
           </div>
           <RickAndMorty/>
       </div>
    );
};

export default AllCountryData;
